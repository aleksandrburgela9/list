<?php

namespace App;

include('ListInterface.php');
include('Node.php');

class LinkedList implements ListInterface
{

    private $head;
    private $till;

    public function __construct(array $values = null)
    {
        if(isset($values)){
            foreach ($values as $value){
                $this->insert($value);
            }
        }
    }

    public function insert($value)
    {
        if($this->head == null){
            $firstNode = new Node($value);
            $this->head = $firstNode;
            $this->till = $firstNode;
            return;
        }
        $node = new Node($value);
        $this->till->next = $node;
        $this->till = $node;
    }

    public function show(){
        $index = $this->head;
        while ($index != null){
            print_r(
                'Name: ' . $index->value . '<br>' .
                'Next: ' . (($index->next) ? $index->next->value : 'null') . '<br>' .
                '-----------------------------------------------------------------' . '<br>'
            );
            $index = $index->next;
        }
    }
}